package com.kenfogel.beanexamplesmvn.app;

// Required for Arrays.sort
import com.kenfogel.beanexamplesmvn.data.BookFX;
import com.kenfogel.beanexamplesmvn.data.BookPageComparatorFX;
import java.time.LocalDate;
import java.util.Arrays;
// Required by the Comparator function
import static java.util.Comparator.comparing;

public class BeanTesterFX {

    /**
     * Here is where I am testing my comparisons
     *
     */
    public void perform() {
        // Lets create four books
        BookFX b0 = new BookFX("200", "Xenon", "Hamilton", "Harcourt", 99, LocalDate.of(2017, 2, 4));
        BookFX b1 = new BookFX("500", "Boron", "Bradbury", "Prentice", 108, LocalDate.of(2018, 5, 23));
        BookFX b2 = new BookFX("300", "Radon", "Heinlein", "Thompson", 98, LocalDate.of(2015, 8, 30));
        BookFX b3 = new BookFX("404", "Argon", "Campbell", "Hachette", 102, LocalDate.of(2012, 11, 15));

        // Using Comparable to compare two books
        System.out.println("Value returned by Comparable");
        System.out.println(b0.getTitle() + " compared to " + b1.getTitle() + ": "
                + b0.compareTo(b1));
        System.out.println();

        // Using Comparator to compare two books
        System.out.println("Value returned by Comparator");
        BookPageComparatorFX bookPageComparatorFX = new BookPageComparatorFX();
        System.out.println(b0.getPages() + " compared to " + b1.getPages() + ": "
                + bookPageComparatorFX.compare(b0, b1));
        System.out.println();

        // Create an array we can sort
        BookFX[] myBooks = new BookFX[4];
        myBooks[0] = b0;
        myBooks[1] = b1;
        myBooks[2] = b2;
        myBooks[3] = b3;
        System.out.println("Unsorted");
        displayBooks(myBooks);

        System.out.println("Sorted with Comparable Interface on Title");
        Arrays.sort(myBooks); // uses the Comparable compareTo in the bean
        displayBooks(myBooks);

        System.out.println("Sorted with Comparable Object on Pages");
        Arrays.sort(myBooks, bookPageComparatorFX); // uses the Comparator object
        displayBooks(myBooks);

        System.out.println("Sorted with Comparable lambda expression on Publishers");
        Arrays.sort(myBooks, (s1, s2) -> {
            return s1.getPublisher().compareTo(s2.getPublisher());
        }); // uses the Comparator lambda
        displayBooks(myBooks);

        System.out.println("Sorted with Comparable lambda functions based on ISBN");
        Arrays.sort(myBooks, comparing(BookFX::getIsbn)); // Comparable function
        displayBooks(myBooks);

        System.out.println("Sorted with Comparable lambda functions based on Date Published");
        Arrays.sort(myBooks, comparing(BookFX::getDatePublished)); // Comparable function
        displayBooks(myBooks);

    }

    /**
     * Print the contents of each Book object in the array
     *
     * @param theBooks
     */
    private void displayBooks(BookFX[] theBooks) {
        for (BookFX b : theBooks) {
            System.out.print(b.getIsbn() + "\t");
            System.out.print(b.getTitle() + "\t");
            System.out.print(b.getAuthor() + "\t");
            System.out.print(b.getPublisher() + "\t");
            System.out.println(b.getPages() + "\t");
            System.out.println(b.getDatePublished());
        }
        System.out.println();
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        BeanTesterFX bt = new BeanTesterFX();
        bt.perform();
        System.exit(0);
    }
}
