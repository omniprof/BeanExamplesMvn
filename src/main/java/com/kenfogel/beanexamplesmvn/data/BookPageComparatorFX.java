package com.kenfogel.beanexamplesmvn.data;

import java.util.Comparator;

public class BookPageComparatorFX implements Comparator<BookFX> {

    /**
     * The interface mandated compare method
     *
     * @param book1
     * @param book2
     * @return negative value, 0, or positive value
     */
    @Override
    public int compare(BookFX book1, BookFX book2) {
        return book1.getPages() - book2.getPages();
    }
}
