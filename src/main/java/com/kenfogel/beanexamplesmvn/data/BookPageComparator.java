package com.kenfogel.beanexamplesmvn.data;

import java.util.Comparator;

public class BookPageComparator implements Comparator<Book> {

    /**
     * The interface mandated compare method
     *
     * @param book1
     * @param book2
     * @return negative value, 0, or positive value
     */
    @Override
    public int compare(Book book1, Book book2) {
        return book1.getPages() - book2.getPages();
    }
}
