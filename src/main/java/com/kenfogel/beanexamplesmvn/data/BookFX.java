package com.kenfogel.beanexamplesmvn.data;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BookFX implements Serializable, Comparable<BookFX> {

    private final StringProperty isbn;
    private final StringProperty title;
    private final StringProperty author;
    private final StringProperty publisher;
    private final IntegerProperty pages;
    private final ObjectProperty<LocalDate> datePublished;

    /**
     * Default constructor
     */
    public BookFX() {
        this("", "", "", "", 0, LocalDate.now());
    }

    /**
     * Non-default constructor
     *
     * @param isbn
     * @param title
     * @param author
     * @param publisher
     * @param pages
     * @param datePublished
     */
    public BookFX(final String isbn, final String title, final String author, final String publisher, final int pages, final LocalDate datePublished) {
        this.isbn = new SimpleStringProperty(isbn);
        this.title = new SimpleStringProperty(title);
        this.author = new SimpleStringProperty(author);
        this.publisher = new SimpleStringProperty(publisher);
        this.pages = new SimpleIntegerProperty(pages);
        this.datePublished = new SimpleObjectProperty<>(datePublished);
    }

    public final String getIsbn() {
        return isbn.get();
    }

    public void setIsbn(final String isbn) {
        this.isbn.set(isbn);
    }

    public final StringProperty isbnProperty() {
        return isbn;
    }

    public final String getTitle() {
        return title.get();
    }

    public void setTitle(final String title) {
        this.title.set(title);
    }

    public final StringProperty titleProperty() {
        return title;
    }

    public String getAuthor() {
        return author.get();
    }

    public void setAuthor(final String author) {
        this.author.set(author);
    }

    public final StringProperty authorProperty() {
        return author;
    }

    public final String getPublisher() {
        return publisher.get();
    }

    public void setPublisher(final String publisher) {
        this.publisher.set(publisher);
    }

    public final StringProperty publisherProperty() {
        return publisher;
    }

    public final int getPages() {
        return pages.get();
    }

    public void setPages(final int pages) {
        this.pages.set(pages);
    }

    public final IntegerProperty pagesProperty() {
        return pages;
    }

    public final LocalDate getDatePublished() {
        return datePublished.get();
    }

    public void setDatePublished(final LocalDate datePublished) {
        this.datePublished.set(datePublished);
    }

    public final ObjectProperty<LocalDate> datePublishedProprty() {
        return datePublished;
    }

    @Override
    public String toString() {
        return "Book{" + "isbn=" + isbn.get() + ", title=" + title.get() + ", author=" + author.get() + ", publisher=" + publisher.get() + ", pages=" + pages.get() + ", datePublished=" + datePublished.get() + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.isbn.get());
        hash = 61 * hash + Objects.hashCode(this.title.get());
        hash = 61 * hash + Objects.hashCode(this.author.get());
        hash = 61 * hash + Objects.hashCode(this.publisher.get());
        hash = 61 * hash + this.pages.get();
        hash = 61 * hash + Objects.hashCode(this.datePublished.get());

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BookFX other = (BookFX) obj;
        if (this.pages.get() != other.pages.get()) {
            return false;
        }
        if (!Objects.equals(this.isbn.get(), other.isbn.get())) {
            return false;
        }
        if (!Objects.equals(this.title.get(), other.title.get())) {
            return false;
        }
        if (!Objects.equals(this.author.get(), other.author.get())) {
            return false;
        }
        if (!Objects.equals(this.publisher.get(), other.publisher.get())) {
            return false;
        }
        if (!Objects.equals(this.datePublished.get(), other.datePublished.get())) {
            return false;
        }
        return true;
    }

    /**
     * Natural comparison method for Title
     *
     * @param book
     * @return negative value, 0, or positive value
     */
    @Override
    public int compareTo(BookFX book) {
        return this.title.get().compareTo(book.title.get());
    }
}
